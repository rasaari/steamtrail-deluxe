# Ohjelmistotuotannon ja pelihojelmoinnin yhdistetty projekti

Otsikossa mainituilla kursseilla on projektiluontoinen tehtävä,
joka jälkimmäisen kurssin takia tulee olemaan peliprojekti.

Projektissa tulee työskentelemään kolme henkilöä:
 - Jari Saaranen
 - Simo Krook
 - Kami Nasri

# Määrittely

Projekti tulee perustumaan Jarin ja Simon alkuperäiseen Steamtrail
(https://bitbucket.org/rasaari/steamtrail) projektiin.

Projektin aikana pyritään saamaan mahdollisimman valmis peli aikaiseksi.
Valmiusasteen määrittelee seuraavat vaatimukset.

## Vaatimukset

**Graafisuus**. Pelin on oltava graafinen. Sen saavuttamiseksi käytetään
*OpenGL* teknologiaa.

**Pelattavuus**. Pelissä tulee olemaan tavoite tai tehtävä, jonka
pelaaja voi toteuttaa pelaamalla. Pelaamiseen käytetään näppäimistöä.

# Projektin hallinta

Projektissa käytetään **scrum**-projektinhallintamenetelmää. Yhden
sprintin kestoksi on määritetty yksi viikko.

# Versionhallinta

Versionhallintaan käytetään git-ohjelmaa ja *BitBucket* etärepositoriopalvelua.