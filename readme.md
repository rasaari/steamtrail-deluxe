# Steamtrail-deluxe

# how to set up

Add every *.jar* file from lib folder to classpath.

# how to run

When you have a .jar packed project by name Steamtrail.jar in dist-folder you need to run command
`java -Djava.library.path="lib/native/<platform>" - jar dist/Steamtrail.jar`, where platform is
**windows** or **linux**.
