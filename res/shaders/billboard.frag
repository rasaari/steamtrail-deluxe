#version 120

// @author simokr

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform sampler2D Difftexture2;
uniform sampler2D Difftexture3;

varying float ParticleAlpha, Texunit;

void main (void){
	vec4 diffuse;
	float index = max(0, min(3, floor(Texunit + 0.5)) );
	if(index < 1.0){
                diffuse = texture2D(Difftexture0, gl_PointCoord);
        }
        else if(index < 2.0){
                diffuse = texture2D(Difftexture1, gl_PointCoord);
        }
        else if(index < 3.0){
                diffuse = texture2D(Difftexture2, gl_PointCoord);
        }
        else if(index < 4.0){
                diffuse = texture2D(Difftexture3, gl_PointCoord);
        }
        else{
                diffuse = vec4(0.5,0.5,0.5,1.0);
        }
	
	diffuse.a = diffuse.a*ParticleAlpha;
		
	gl_FragColor = diffuse;
}