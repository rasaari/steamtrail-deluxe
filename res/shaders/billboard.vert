#version 120

// @author simokr

attribute vec3 OBJ_Position;
attribute vec3 OBJ_Normal;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;

varying float ParticleAlpha, Texunit;

void main (void){
		gl_PointSize = OBJ_Normal.x;
		ParticleAlpha = OBJ_Normal.y;
		Texunit = OBJ_Normal.z;
		
		gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1);
}
