#version 120

// @author simokr

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform sampler2D Difftexture2;
uniform sampler2D Difftexture3;

varying vec2 Texcoord;
varying float Texunit;
varying vec3 eyePos, eyeNormal, lightDir;

void main (void){
	vec3 eye = normalize(eyePos);
        
	float index = max(0, min(3, floor(Texunit + 0.5)) );
        vec4 diffuse;
	vec4 specular = vec4(0.0);
	vec4 ambient = vec4(0.4,0.4,0.4,1.0);
		
		
	float shininess = 55.0;
	vec4 specularColor = vec4(0.5, 0.5, 0.5, 1);
	
	float intensity = max(dot(eyeNormal, lightDir), 0.0);
		
	
		
        if(index < 1.0){
                diffuse = texture2D(Difftexture0, Texcoord);
        }
        else if(index < 2.0){
                diffuse = texture2D(Difftexture1, Texcoord);
        }
        else if(index < 3.0){
                diffuse = texture2D(Difftexture2, Texcoord);
        }
        else if(index < 4.0){
                diffuse = texture2D(Difftexture3, Texcoord);
        }
        else{
                diffuse = vec4(0.5,0.5,0.5,1.0);
        }
        
	if(intensity > 0.0){
		vec3 h = normalize(lightDir + eye);
		float intSpec = max(dot(h,eyeNormal), 0.0);
		specular = vec4(vec3(diffuse.a),1) * pow(intSpec, shininess);
	}
        
        gl_FragColor = ambient * diffuse + intensity * diffuse + specular;
}