#version 120

// @author simokr

attribute vec3 OBJ_Position;
attribute vec2 OBJ_Texcoord;
attribute vec3 OBJ_Normal;
attribute float OBJ_TexUnit;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;

varying float Texunit;
varying vec2 Texcoord;
varying vec3 Normal;
varying vec3 eyePos, eyeNormal;
varying vec3 lightDir;

void main (void){
        Texcoord = OBJ_Texcoord;
        Texunit = OBJ_TexUnit;
        Normal = OBJ_Normal;
	lightDir = mat3(ViewMatrix) * normalize(vec3(1,1,1));
	
	eyePos = vec3(ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1));
	eyeNormal = normalize(vec3(ViewMatrix * ModelMatrix * vec4(OBJ_Normal, 0)));
        gl_Position = ProjectionMatrix * vec4(eyePos, 1);
	eyePos = -eyePos;
}
