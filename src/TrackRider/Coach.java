/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackRider;

import engine.Engine;
import engine.Map;
import graphics.model.Model;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class Coach extends TrackRider {
	public static final int PASSENGER = 0;
	public static final int COAL = 1;
        
        // is the coach loaded after stopping to a station
        private boolean loaded;
	
	public Coach(Map map) {
		// Model specific initialization
		
		this.model = new Model();
		this.model.setMesh(Engine.MeshHandler.get("res/mesh/coach1.obj"));
		
		// 61 tons
		this.setWeight(61*1000);
		
		this.setMap(map);
		
		this.setLocation(new Vector2f(10, 10));
                
		// be default, train is loaded
		this.loaded = true;
	}

	public boolean isLoaded() {
		return this.loaded;
	}
	
	public void load() {
		this.loaded = true;
	}
	
	@Override
	public void render() {
		getModel().render();
		
		if(this.coach != null) {
			this.coach.render();
		}
	}

	@Override
	public void update() {
		updateModel();
		
		//System.out.println(this.getVelocity() +" - " + this.isLoaded());
		
		if(this.getVelocity() > 0f && loaded) {
			this.loaded = false;
		}
                
		if(this.coach != null) {
			this.coach.setVelocity(this.getVelocity());
			this.coach.update();
		}
	}

	@Override
	protected void onTurn() {
		if(this.coach != null) {
			this.coach.requestTurn(this.getInterSectionDirection());
		}
	}

	@Override
	public void free() {
	}
}
