/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackRider;

import engine.Engine;
import engine.Map;
import graphics.model.Model;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class CoalCoach extends Coach {
	// estimated average weight of one coal
	private final float coalWeight = 0.1f;
	
	// actual amount of coals this coach is carrying
	private long coals;
	
	// maximum amount of coals
	private long capacity;
	
	public CoalCoach(Map map) {
		super(map);
		
		// Model specific initialization
		this.model = new Model();
		this.model.setMesh(Engine.MeshHandler.get("res/mesh/coalcoach.obj"));

		// 20 tons
		this.setWeight(20*1000);
		this.capacity = 15*1000;
		this.addCoals(3*1000);
		
		this.setMap(map);
		
		this.setLocation(new Vector2f(10, 10));
	}
	
	@Override
	public void update(){
		super.update();
		this.model.animate(1.0f-(this.getCoals()/(float)this.getCapacity()), 0, 1);
		
	}
	
	@Override
	public void render(){
		super.render();
	}
	
	@Override
	public float getWeight() {
		return super.getWeight() + coals*coalWeight;
	}
	
	public void addCoals(long amount) {
		this.coals += amount;
		if(this.coals > capacity)
			this.coals = capacity;
	}
	
	public void useCoals(long amount) {
		this.coals -= amount;
		if(this.coals < 0) 
			this.coals = 0;
	}
	
	public long getCoals() {
		return this.coals;
	}

	public long getCapacity() {
		return this.capacity;
	}
	
	
}
