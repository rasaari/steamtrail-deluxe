package TrackRider;

import engine.Map;
import engine.Passenger;
import engine.Station;
import graphics.emitter.Emitter;
import graphics.material.Material;
import java.util.ArrayList;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class PassengerCoach extends Coach {
	private final int DEFAULT_CAPACITY = 40;
	
	// how many passenger the coach can hold
	private int capacity;
	
	// how many passenger there are aboard now
	private ArrayList<Passenger> passengers;
	
	// total weight of the passengers. updated when needed to reduce
	// totally pointless calculations.
	private float passengerWeightCache;
	
	private Emitter passengerEmitter;
	
	PassengerCoach(Map map) {
		super(map);
		Material smileyMat = new Material();
		smileyMat.setTexture(0, "res/texture/smiley.png");
		smileyMat.setTexture(1, "res/texture/frown.png");
		
		passengerEmitter = new Emitter(10, 4000);
		passengerEmitter.setMaterial(smileyMat);
		passengerEmitter.setVelocity(new Vector3f(0,0.5f,0));
		passengerEmitter.setSizes(15, 40);
		passengerEmitter.setSizesRandomness(5, 10);
		passengerEmitter.setTranslation(new Vector3f(0,0.6f,0));
		passengerEmitter.setStartPositionRandomness(new Vector3f(0.25f,0.15f,0.25f));
		passengerEmitter.pause();
		
		this.capacity = DEFAULT_CAPACITY;
		this.passengers = new ArrayList();
		this.calculateTotalPassengerWeight();
	}
	
	@Override
	public float getWeight() {
		return super.getWeight() + passengerWeightCache;
	}
	
	public void addPassenger(Passenger passenger) {
		if(this.passengers.size() < this.capacity) {
			this.passengers.add(passenger);
			this.calculateTotalPassengerWeight();
		}
	}
	
	public void addPassengers(int amount) {
		/*
		this.passengers += amount;
		if(this.passengers > this.capacity)
			this.passengers = this.capacity;
		*/
	}
	
	public void removePassengers(int amount) {
		/*
		this.passengers -= amount;
		if(this.passengers < 0)
			this.passengers = 0;
		*/
	}
	
	public ArrayList<Passenger> getPassengers() {
		return this.passengers;
	}
	
	public int getPassengerCount() {
		return this.passengers.size();
	}
	
	@Override
	public void update() {
		super.update();
		this.passengerEmitter.setPosition(this.model.getPosition());
		this.passengerEmitter.update();

		if(this.getVelocity() == 0f && !this.isLoaded()) {
			if(this.map.get(this.getGridLocation().x, this.getGridLocation().y) == Map.STATION) {
				unload();
				load();
			}
		}
	}
	
	@Override
	public void render(){
		super.render();
		
		this.passengerEmitter.render();
	}
	
	public void unload() {
		Station station = this.map.getStationAtTile(this.getGridLocation());
		
		if(station != null) {
			ArrayList<Passenger> remove = new ArrayList();
			
			for(Passenger passenger : this.getPassengers()) {
				if(passenger.getDestination().equals(station)) {
					remove.add(passenger);
					if(!passenger.isLate()) {
						this.addCash(passenger.getTicketPrice());
						passengerEmitter.emit(0);
					}
					else {
						passengerEmitter.emit(1);
						//System.out.println("Passenger was late and didn't pay.");
					}
				}
			}
			
			for(Passenger p: remove) {
				this.getPassengers().remove(p);
			}
			
			this.calculateTotalPassengerWeight();
		}
	}
	
	@Override
	public void load() {
		super.load();
		//this.removePassengers((int) (Math.random()*this.capacity));
		//this.addPassengers((int) (Math.random()*this.capacity));
		Station station = this.map.getStationAtTile(this.getGridLocation());
		
		if(station != null) {
			ArrayList<Passenger> remove = new ArrayList();
			for(Passenger p: station.getPassengers()) {
				if(this.getPassengerCount() < this.capacity) {
					this.addPassenger(p);
					//this.addCash(2.50f);
					remove.add(p);
				}
			}
			
			for(Passenger p: remove) {
				station.getPassengers().remove(p);
			}
		}
	}
	
	private void calculateTotalPassengerWeight() {
		this.passengerWeightCache = 0.0f;
		
		for(Passenger p: this.passengers) {
			this.passengerWeightCache += p.getWeight();
		}
	}
	
	@Override
	public void free(){
		passengerEmitter.free();
	}
	
}
