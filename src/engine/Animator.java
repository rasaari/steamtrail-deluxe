package engine;

/**
 *
 * @author jari
 */
public class Animator {
	private int frameCount = 0;
	private float timeScale = 1.0f;
	private float animationPosition = 0.0f;
	public boolean reverse = false;
	
	public Animator(int frameCount, float timeScale) {
		this.frameCount = frameCount;
		this.timeScale = timeScale;
	}
	
	public void update() {
		if(!reverse) {
			animationPosition += Engine.appSpeed()*timeScale;
		}
		
		else {
			animationPosition -= Engine.appSpeed()*timeScale;
		}
		
		if( (int)animationPosition > frameCount ) {
			animationPosition = 0.0f;
		}
		
		if( (int)animationPosition < 0 ) {
			animationPosition = frameCount-1;
		}
		
	}
	
	public float getFrameBend() {
		return animationPosition - (float)Math.floor(animationPosition);
	}
	
	public int getFrame() {
		return (int)Math.floor(animationPosition);
	}
	
	public void setTimeScale(float timeScale) {
		this.timeScale = timeScale;
	}
}
