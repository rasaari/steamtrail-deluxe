package engine;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.nulldevice.NullInputSystem;
import de.lessvoid.nifty.nulldevice.NullSoundDevice;
import de.lessvoid.nifty.renderer.lwjgl.render.LwjglRenderDevice;
import de.lessvoid.nifty.renderer.lwjgl.time.LWJGLTimeProvider;
import static org.lwjgl.opengl.GL11.*;
import graphics.Graphics;
import graphics.MeshHandler;
import graphics.ShaderHandler;
import graphics.mesh.Mesh;
import graphics.shader.Shader;
import graphics.TextureHandler;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;
import state.Intro;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 * @author simokr
 */
public class Engine {
	//engine state

	private boolean running;
	private static float appSpeed;
	private long previousTime, currentTime;
	//gui
	private Nifty nifty;
	public static final Logger LOGGER = Logger.getLogger(Graphics.class.getName());
	// Resource handlers. Initialization in init
	public static TextureHandler<TextureImpl, Integer> TextureHandler;
	public static MeshHandler<Mesh, Mesh> MeshHandler;
	public static ShaderHandler<Shader, Shader> ShaderHandler;
	private static long elapsedAppTime;
	private static boolean isPaused = false;
	
	static {
		try {
			LOGGER.addHandler(new FileHandler("errors.log", true));
		}
		catch (IOException ex) {
			LOGGER.log(Level.WARNING, ex.toString(), ex);
		}
	}

	public Engine() {
	}

	public int run() {


		State state = StateManager.getState();
		state.setGui(nifty);
		
		// initialize main loop
		running = true;
		this.currentTime = System.currentTimeMillis();
		elapsedAppTime = 0;
		
		while (running && !Display.isCloseRequested()) {
			//update timestamp
			this.previousTime = this.currentTime;
			this.currentTime = System.currentTimeMillis();
			
			if(!isPaused)
				elapsedAppTime += this.currentTime-this.previousTime;
			
			//calculate new appSpeed (deltatime?)
			appSpeed = 60f / (1000.0f / (float) (this.currentTime - this.previousTime));

			//read current gamestate
			state = StateManager.getState();
			
			if(!isPaused)
				state.update();
			else
				state.paused();
			
			nifty.update();

			// Pass current State's camera to Graphics
			Graphics.setCurrentCamera(state.getCamera());
			
			if (Display.isVisible()) {
				this.render();
			}
			else {
				if (Display.isDirty()) {
					this.render();
				}
				try {
					Thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
			}

			Display.update();
			Display.sync(60);
		}

		return 0;
	}

	public void start() {
		try {
			Graphics.create();
			this.init();

			System.gc();

			this.run();
		}
		catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		finally {
			Graphics.destroy();
		}
	}

	private void init() {
		TextureHandler = new TextureHandler<>();
		MeshHandler = new MeshHandler<>();
		ShaderHandler = new ShaderHandler<>();

		StateManager.init();
		StateManager.push((State) (new Intro()));

		// create new Nifty userinterface
		nifty = new Nifty(
			new LwjglRenderDevice(false),
			new NullSoundDevice(),
			new NullInputSystem(),
			new LWJGLTimeProvider());
		
		// load GUI from file
		nifty.addXml("res/xml/IntroScreen.xml");
		nifty.addXml("res/xml/GameScreen.xml");
		nifty.addXml("res/xml/EndScreen.xml");
	}

	public static float appSpeed() {
		return appSpeed;
	}
	
	public static long elapsedAppTime() {
		return elapsedAppTime;
	}

	public static boolean isPaused() {
		return isPaused;
	}
	
	public static void resume() {
		isPaused = false;
	}
	
	public static void pause() {
		isPaused = true;
	}
	

	private void render() {
		Graphics.clear();

		Graphics.set3dProjection();
		// Game world rendering

		State state = StateManager.getState();
		state.render();

		glPushAttrib(GL_ALL_ATTRIB_BITS);
		Graphics.set2dProjection();
		//Nifty rendering
		nifty.render(false);

		// Element niftyElement = nifty.getCurrentScreen().findElementByName("velocity");
		//niftyElement.getRenderer(TextRenderer.class).setText("Velocity: ");

		glPopAttrib();
	}
}
