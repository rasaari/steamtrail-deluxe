package engine;

import graphics.model.Model;
import java.awt.Point;
import org.lwjgl.util.vector.Vector2f;

/**
 * 
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class GameObject {
	
	private Vector2f location;
	private int direction;
	
	protected Model model;

	public GameObject() {
		location = new Vector2f(0,0);
		direction = 0;
	}
	
	//location methods
	public void setLocation(Vector2f location) { this.location.set(location); }
	public Vector2f getLocation() { return this.location; }
	
	public Point getGridLocation() {
		return new Point(Math.round(this.getLocation().x), Math.round(this.getLocation().y));
	}
	
	//direction methods
	public void setDirection(int direction) { this.direction = direction%8; }
	public int getDirection() { return this.direction; }
	
	/**
	 * Turn object by defined amount.
	 * @param amount 
	 */
	public void turn(int amount) { 
		/* add amount to direction and make sure direction stays
		 * within limits. */
		this.direction += amount; 
		this.direction = this.direction%8;
		if(this.direction < 0) this.direction += 8;
	}
	
	public int getTurnDirection(int amount) {
		int dir = this.direction+amount;
		dir = dir%8;
		if(dir < 0) dir += 8;
		
		return dir;
	}
	
	//model
	public Model getModel() { return this.model; }
}
