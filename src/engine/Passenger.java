package engine;

import org.lwjgl.util.vector.Vector2f;
import static engine.Engine.elapsedAppTime;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class Passenger {
	private final float ticketBasePrice = 2.2f;
	private final float distanceFactor = 0.08f;
	
	// 6 minutes
	private final long baseTravelTime = 6*60*1000;
	
	// weight of the passenger in kilograms
	private float weight;
	
	// destination station
	private Station destination;
	
	private float ticketPrice;
	private long arrivalTime;
	
	public Passenger() {
		this.weight = (float) (Math.random()*60+50);
		this.ticketPrice = 0.0f;
	}
	
	public float getWeight() {
		return this.weight;
	}
	
	public void setDestination(Vector2f currentLocation, Station destination) {
		this.destination = destination;
		
		float distance = (float) Math.sqrt(
			Math.pow(Math.abs(currentLocation.x-destination.getCenterPosition().x), 2)+
			Math.pow(Math.abs(currentLocation.y-destination.getCenterPosition().y), 2)
		);
		
		this.ticketPrice = ticketBasePrice*(distanceFactor*distance);
		
		// time to reach the destination
		this.arrivalTime = (long) (elapsedAppTime() + baseTravelTime + Math.random()*2*60*1000);
	}
	
	public Station getDestination() {
		return this.destination;
	}
	
	public float getTicketPrice() {
		return this.ticketPrice;
	}
	
	public boolean isLate() {
		return this.arrivalTime < elapsedAppTime();
	}
}
