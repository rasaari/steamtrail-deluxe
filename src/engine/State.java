package engine;

import de.lessvoid.nifty.Nifty;
import graphics.Camera;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public abstract class State {
	protected Camera camera;
	protected Nifty gui = null;
	
	public State(){
		camera = new Camera();
	}
	
	abstract public void init();
	abstract public void end();
        abstract public void initGui();
	abstract public void render();
	abstract public void update();
	abstract public void paused();
	
	//todo: make some sense out of this
	abstract public void event();
	
	public Camera getCamera(){
		return this.camera;
	}
	
	public boolean hasGui() {
		return (gui != null);
	}
	
	public void setGui(Nifty gui) {
		this.gui = gui;
		initGui();
	}
}
