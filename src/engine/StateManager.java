package engine;

import java.util.Stack;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class StateManager {
	private static StateManager instance;
	private Stack<State> states;
	
	//initialize state manager
	public static void init() {
		if (instance == null)
			instance = new StateManager();
		
		//initialize state stack
		instance.states = new Stack<State>();
	}
	
	//push new state to state manager
	public static void push(State state) {
		instance.states.push(state);
		
		if(instance.states.size() > 1)
			getState().setGui(instance.states.get(0).gui);
		
		//initialize state after it is pushed
		getState().init();
	}
	
	//pop state from state manager and return it
	public static State pop() {
		State popped = instance.states.pop();
		popped.end();
		
		return popped;
	}
	
	//return current state
	public static State getState() {
		return instance.states.peek();
	}
}
