package engine;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 * @author simokr
 */
public class Station {
	private String name;
	private ArrayList<Point> tiles;
	private ArrayList<Passenger> departingPassengers;
	private Vector2f centerPosition;
	
	public Station(){
		name = "null";
		tiles = new ArrayList<>();
		departingPassengers = new ArrayList();
		centerPosition = new Vector2f();
		
		this.generateName();
	}
	
	public void addStationTile(Point tile){
		if(!this.tiles.contains(tile)){
			this.tiles.add(tile);
			
			this.centerPosition.set(0f, 0f);
			for (Point t : tiles) {
				this.centerPosition.x += t.x;
				this.centerPosition.y += t.y;
			}
			this.centerPosition.x /= tiles.size();
			this.centerPosition.y /= tiles.size();
		}
	}
	
	public boolean hasTile(Point tile){
		if(this.tiles.contains(tile))
			return true;
		
		return false;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void addPassenger(Passenger passenger) {
		this.departingPassengers.add(passenger);
	}
	
	public ArrayList<Passenger> getPassengers() {
		return this.departingPassengers;
	}
	
	public Vector2f getCenterPosition(){
		return new Vector2f(this.centerPosition);

	}
	
	private void generateName() {
		String[] prefix = {
			"Great ",
			"Little ",
			"New ",
			"Fort ",
			"St. ",
			"Old "
		};
		
		String[] pre = {
			"Boom",
			"Crumble",
			"Flat",
			"Leaf",
			"Moon",
			"Morning",
			"North",
			"Tera",
			"Tree",
			"South",
			"Ward",
			"Warding",
			"Troll",
			"Stone",
			"Elektro"
		};
		
		String[] post = {
			"castle",
			"chester",
			"ford",
			"fort",
			"ham",
			"ston",
			"ton",
			"blocks",
			"burg",
			"gorsk",
			"zavodsk"
		};
		
		String[] extra = {
			"-on-sea",
			" Ridge",
			" Woods",
			" Plains",
			" Market",
			" Cross",
			" Falls",
			" City",
			" Springs"
		};
		
		Random rand = new Random();
		
		this.name = pre[rand.nextInt(pre.length)];
		
		if(rand.nextFloat() > 0.80f){
			this.name =  prefix[rand.nextInt(prefix.length)] + this.name;
		}
		
		this.name += post[rand.nextInt(post.length)];
		
		if(rand.nextFloat() > 0.80f){
			this.name +=  extra[rand.nextInt(extra.length)];
		}
		
		System.out.println(name + " was founded!");
	}
	
	@Override
	public String toString(){
		String str = "Station: "+this.name+", at: ("+this.centerPosition.x+","+this.centerPosition.y+"), departing passengers: "+this.departingPassengers.size();
		return str;
	}
}
