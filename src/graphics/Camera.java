package graphics;

import org.lwjgl.util.vector.ReadableVector3f;
import org.lwjgl.util.vector.Vector3f;

/**
 * 
 * @author simokr
 */
public class Camera {
	private Vector3f position, rotation;
	private int fieldOfView;
	
	public Camera(){
		position = new Vector3f();
		rotation = new Vector3f();
		fieldOfView = 85;
	}
	
	public ReadableVector3f getPosition(){
		return this.position;
	}
	
	public ReadableVector3f getRotation(){
		return this.rotation;
	}
	
	public float getFieldOfView(){
		return this.fieldOfView;
	}
	
	public void setPosition(float x, float y, float z){
		this.position.set(x, y, z);
	}
	
	public void setRotation(float x, float y, float z){
		this.rotation.set(x, y, z);
	}
	
	public void setFieldOfView(int fov){
		this.fieldOfView = Math.max(10, Math.min(100, fov));
	}
	
	public void lookAt(float x, float y, float z){
		Vector3f forward = new Vector3f(x-this.position.x, y-this.position.y, z-this.position.z);
		forward.normalise();

		this.rotation.x = (float) Math.toDegrees(Math.asin(-forward.y));
		this.rotation.y = (float) Math.toDegrees(Math.PI/2+Math.atan2(forward.z, forward.x));
	}
}
