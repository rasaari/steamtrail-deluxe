package graphics;

import graphics.mesh.Mesh;
import graphics.mesh.ObjMesh;

/**
 * 
 * @author simokr
 * @param <R> graphics.mesh.Mesh
 * @param <E> graphics.mesh.Mesh
 */
public class MeshHandler<R extends Mesh, E extends R> extends ResourceHandler<R,E>{

	public MeshHandler() {
		super();
	}
	
	@Override
	protected E getExternal(String name) {
		E value = (E)this.resources.get(name);
		return value;
	}

	@Override
	protected R load(String path) {
		ObjMesh mesh = new ObjMesh();
		
		if(!(mesh.create(path, true)))
			return null;
		
		return (R)mesh;
	}

	@Override
	protected void freeIt(R resource) {
		resource.free();
	}
}
