package graphics;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author simokr
 * @param <R> Resource to be stored internally
 * @param <E> Resource to be given externally
 */
public abstract class ResourceHandler<R,E>{
	protected HashMap<String,R> resources;

	public ResourceHandler() {
		resources = new HashMap<>();
	}
	
	public E get(String path){
		if(path == null || path.trim().isEmpty()) return null;
		
		path = path.trim();
		
		if(this.resources.containsKey(path) == false){
			/* Resource hasn't been loaded already*/
			
			if(!this.add(path)){
				/* Loading the resource failed */
				System.err.println("Failed to load resource(s): " + path);
				this.resources.put(path, null);
			}
			else{
				/* Loading the resource succeeded */
				//System.out.println("Added resource: " + path);
			}
			
		}
		
		return this.getExternal(path);
	}
	
	abstract protected E getExternal(String name);
	
	protected boolean add(String path){
		
		String fileList[] = path.split("\\|");
		for (String oneFile : fileList) {
			File file = new File(oneFile.trim());

			if(!file.exists())
				return false;
		}
		
		R resource;
		
		resource = this.load(path);
		if(resource == null)
			return false;
		
		resources.put(path, resource);
		return true;
	}
	
	abstract protected R load(String path);
	
	public void freeAll(){
		Set<Entry<String,R>> ent = this.resources.entrySet();
		Iterator<Entry<String,R>> iterator = ent.iterator();
		while(iterator.hasNext()){
			Entry<String,R> element = iterator.next();
			if(!element.getKey().equals("empty")){
				this.freeIt(element.getValue());
				iterator.remove();
			}
		}
		
	}
	
	public void free(String path){
		if(path == null || path.trim().isEmpty()) return;
			path = path.trim();
			
		if(this.resources.containsKey(path) == true){
			R resource = this.resources.get(path);
			this.freeIt(resource);
			this.resources.remove(path);
			System.out.println("Freed resource: " + path);
		}
	}
	
	abstract protected void freeIt(R resource);
}
