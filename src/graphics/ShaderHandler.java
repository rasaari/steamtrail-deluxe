package graphics;

import graphics.shader.Shader;
import java.util.Collection;

/**
 * @author simokr
 * @param <R> graphics.shader.Shader
 * @param <E> graphics.shader.Shader
 */
public class ShaderHandler<R extends Shader, E extends R> extends ResourceHandler<R,E>{
	public ShaderHandler() {
		super();
	}
	
	@Override
	protected E getExternal(String name) {
		E value = (E)this.resources.get(name);
		return value;
	}

	@Override
	protected R load(String path) {
		R shader = (R)new Shader();
		String shaderPaths[] = path.split("\\|");
		if(shaderPaths.length != 2)
			return null;
		
		if(!shader.create(shaderPaths[0], shaderPaths[1]))
			return null;
		
		return shader;
	}

	@Override
	protected void freeIt(R resource) {
		resource.free();
	}
	
	public void reloadAll(){
		Collection<R> shaders = this.resources.values();
		for(Shader oneShader: shaders){
			oneShader.refresh();
		}
	}
}
