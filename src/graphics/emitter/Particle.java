package graphics.emitter;

import org.lwjgl.util.vector.Vector3f;
import static engine.Engine.appSpeed;
import static graphics.Graphics.getCurrentCamera;
import org.lwjgl.util.vector.ReadableVector3f;

/**
 *
 * @author simokr
 */
public class Particle implements Comparable<Particle>{
	public Vector3f position;
	private Vector3f velocity;
	public float size;
	private float startSize, endSize;
	private float sizeDelta;
	private long life, end;
	private float alpha, textureUnit;
	
	public Particle(long start, long end, float startSize, float endSize, ReadableVector3f position, ReadableVector3f velocity){
		this.position = new Vector3f(position);
		this.velocity = new Vector3f(velocity);
		this.life = end-start;
		this.end = end;
		this.startSize = startSize;
		this.endSize = endSize;
		sizeDelta = this.endSize - this.startSize;
		size = startSize;
		alpha = 1.0f;
		textureUnit = 0;
	}
	
	public boolean isOld(long currentTime){
		return currentTime > this.end;
	}
	
	public void update(ReadableVector3f acceleration, long currentTime){
		float onePerSecond = appSpeed()/60;
		this.velocity.x += acceleration.getX() * onePerSecond;
		this.velocity.y += acceleration.getY() * onePerSecond;
		this.velocity.z += acceleration.getZ() * onePerSecond;
		
		this.position.x += this.velocity.x * onePerSecond;
		this.position.y += this.velocity.y * onePerSecond;
		this.position.z += this.velocity.z * onePerSecond;
		
		this.alpha = (float)(this.end-currentTime)/this.life;
		this.size = this.startSize+this.sizeDelta*(1f-(this.alpha));
		this.alpha = (float) Math.min(1.0,Math.pow(2*this.alpha, 4));
	}

	@Override
	public int compareTo(Particle o) {
		float distance = o.distanceToCamera();
		float distanceOther = this.distanceToCamera();
		return (distance > distanceOther)?1:(distance == distanceOther)?0:-1;
	}
	
	public float distanceToCamera(){
		ReadableVector3f camPos = getCurrentCamera().getPosition();
		float distance = new Vector3f(this.position.x-camPos.getX(), this.position.y-camPos.getY(), this.position.z-camPos.getZ()).length();
		return distance;
	}
	
	public float getAlpha(){
		return this.alpha;
	}
	
	public float getTextureUnit(){
		return this.textureUnit;
	}
	
	public void setTextureUnit(int unit){
		this.textureUnit = Math.max(0, unit);
	}
}
