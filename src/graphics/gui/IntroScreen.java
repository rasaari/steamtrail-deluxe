/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics.gui;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author Kami Nasri
 */
public class IntroScreen implements ScreenController{
	
	private Nifty nifty;
	private Screen screen;
	
	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
	}

	@Override
	public void onStartScreen() {
	}

	@Override
	public void onEndScreen() {
		
	}
	
}
