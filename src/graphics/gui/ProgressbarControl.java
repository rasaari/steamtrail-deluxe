/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics.gui;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Controller;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.input.NiftyInputEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.tools.SizeValue;
import de.lessvoid.xml.xpp3.Attributes;
import java.util.Properties;

/**
 *
 * @author Kami Nasri
 */
public class ProgressbarControl implements Controller {

    private final int MIN_WIDTH = 16;
    private int pixelWidth;
    private Element progressBarElement;

    @Override
    public void bind(Nifty nifty, Screen screen, Element elmnt, Properties prprts, Attributes atrbts) {
		//Blue progressbar
		if(elmnt.getId().equalsIgnoreCase("progressbar_power") || elmnt.getId().equalsIgnoreCase("progressbar_heat"))
			progressBarElement = elmnt.findElementByName("progress");
		
		//Red progressbar
		if(elmnt.getId().equalsIgnoreCase("progressbar_damage"))
			progressBarElement = elmnt.findElementByName("progress_damage");
    }

    @Override
    public void init(Properties prprts, Attributes atrbts) {
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onFocus(final boolean getFocus) {
    }

    @Override
    public boolean inputEvent(final NiftyInputEvent inputEvent) {
        return false;
    }
    
    public void setProgress(final float progressValue) {
        float progress = progressValue;

        if (progress < 0.0f) progress = 0.0f;
        else if (progress > 1.0f) progress = 1.0f;

        pixelWidth = (int) (MIN_WIDTH + (progressBarElement.getParent().getWidth() - MIN_WIDTH) * progress);
        progressBarElement.setConstraintWidth(new SizeValue(pixelWidth + "px"));
        progressBarElement.getParent().layoutElements();
    }
}
