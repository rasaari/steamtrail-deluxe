package graphics.material;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static engine.Engine.TextureHandler;
import graphics.mesh.ObjMesh;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simokr
 */
public class Material {
	/**
	 * Limit to how many textures can be passed to shaders
	 */
	private final int maxTextures = 4;
	private int textures[];
	
	public Material(){
		textures = new int[maxTextures];
		for (int i = 0; i < maxTextures; i++) {
			textures[i] = 0;
		}
		
	}
	
	/**
	 * Material copy constructor
	 * 
	 * @param old the Material to be copied
	 */
	public Material(Material old) {
		textures = new int[maxTextures];
		for (int i = 0; i < maxTextures; i++) {
			textures[i] = old.textures[i];
		}
	}
	
	/**
	 * Takes in a list of Materials and creates a new compact Material with no
	 * duplicate textures.
	 * 
	 * Textures that go over the material texture limit are dropped.
	 * 
	 * @param mats A list of materials
	 */
	public Material(ArrayList<Material> mats){
		textures = new int[maxTextures];
		for (int i = 0; i < maxTextures; i++) {
			textures[i] = 0;
		}
		int lastSetTextureIndex = 0;
		for (Material material : mats) {
			for (int i = 0; i < maxTextures; i++) {
				int handle = material.getTextureHandle(i);
				
				if(handle == 0)
					break;
				
				boolean foundInThis = false;
				for (int t = 0; t < maxTextures; t++) {
					if(handle == this.getTextureHandle(t)){
						foundInThis = true;
						break;
					}
				}
				
				if(!foundInThis){
					this.textures[lastSetTextureIndex++] = handle;
					
					/* We have ran out of texture slots */ 
					if(lastSetTextureIndex >= this.maxTextures)
						return;
				}
			}
			
		}
	}
	
	/**
	 * Sets a texture to the given texture slot.
	 * Slots that go over the slot limit are ignored.
	 * A grey texture will be used if the given texture fails to load.
	 * 
	 * @param index texture slot to be set
	 * @param path path to texture file
	 */
	public void setTexture(int index, String path){
		if(index >= 0 && index < this.maxTextures){
			this.textures[index] = TextureHandler.get(path);
		}
	}
	/**
	 * Returns the texture handle of the given slot.
	 * Slots that go over the slot limit return 0.
	 * 
	 * @param index texture slot to be checked
	 * @return handle
	 */
	public int getTextureHandle(int index){
		if(index >= 0 && index < this.maxTextures){
			return this.textures[index];
		}
		
		return 0;
	}
	
	/**
	 * Finds the texture index that the given texture handle has in this Material.
	 * Slots that go over the slot limit return -1.
	 * 
	 * @param handle the handle to look for
	 * @return the index or -1 if the handle wasn't found
	 */
	public int getTextureHandleIndex(int handle){
		for (int i = 0; i < this.maxTextures; i++) {
			if(textures[i] == handle){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Clears the given texture slot.
	 * Slots that go over the slot limit are ignored.
	 * 
	 * @param index slot to be cleared
	 */
	public void clearTexture(int index){
		if(index >= 0 && index < this.maxTextures){
			this.textures[index] = 0;
		}
	}
	
	/**
	 * Binds all the textures to OpenGL
	 */
	public void bindTextures(){
		for (int i = 0; i < this.maxTextures; i++) {
			if(textures[i] == 0)
				continue;
			
			glActiveTexture(GL_TEXTURE0+i);
			glBindTexture(GL_TEXTURE_2D, this.textures[i]);
		}
	}
	
	/**
	 * Releases all the texture binds from OpenGL
	 */
	public void unbindTextures(){
		for (int i = 0; i < this.maxTextures; i++) {
			if(textures[this.maxTextures-1-i] == 0)
				continue;
			
			glBindTexture(GL_TEXTURE_2D, 0);
			if(i != this.maxTextures-1)
				glActiveTexture(GL_TEXTURE2-i);
		}
	}
	
	private LinkedHashMap<String,String> getMtlTextures(String materialLib) throws FileNotFoundException{
		File file = new File(materialLib);
		if(!file.exists()){
			throw new FileNotFoundException();
		}
		LinkedHashMap<String,String> mtlMaterials = new LinkedHashMap<>();
		
		ArrayList<String> data = ObjMesh.readFile(materialLib);
		
		/* .mtl was empty */
		if(data.isEmpty())
			return mtlMaterials;
	
		String line;
		for(int i=0; i<data.size(); i++){
			line = data.get(i).trim();
			String str[] = line.split("\\s+");
			
			if(line.startsWith("newmtl ")){
				if(str.length > 1){
					String texture = this.getNextTexture(data, i+1);
					if(!texture.isEmpty()){
						mtlMaterials.put(str[1].trim(), texture);
					}
				}
			}
		}
		
		return mtlMaterials;
	}
	
	/**
	 * Reads the given .mtl file and loads all materials from it.
	 * 
	 * @param materialLib path to .mtl
	 * @return success
	 */
	public boolean loadFromFile(String materialLib) {
		LinkedHashMap<String,String> mtlMaterials;
		
		try {
			mtlMaterials = this.getMtlTextures(materialLib);
		}
		catch (FileNotFoundException ex) {
			Logger.getLogger(Material.class.getName()).log(Level.SEVERE, ".mtl file not found!", ex);
			return false;
		}
		
		if(mtlMaterials.isEmpty())
			return false;
		
		for (int i = 0; i < maxTextures; i++) {
			textures[i] = 0;
		}
		
		Collection<String> textureList = mtlMaterials.values();
		
		int i = 0;
		for (String oneTexture : textureList) {
			this.setTexture(i++, "res/texture/" + oneTexture);
		}
		
		return true;
	}
	
	/**
	 * Version of the method that loads all materials from the given .mtl but filters them by materials found in .obj
	 * 
	 * @param objMaterials list of material names to be loaded
	 * @param materialLib path to .mtl
	 * @return success
	 */
	public boolean loadFromFile(ArrayList<String> objMaterials, String materialLib) {
		LinkedHashMap<String,String> mtlMaterials;
		
		try {
			mtlMaterials = this.getMtlTextures(materialLib);
		}
		catch (FileNotFoundException ex) {
			Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		
		/* No valid material & texture combinations were found in .mtl */
		if(mtlMaterials.isEmpty())
			return false;
		
		for (int i = 0; i < maxTextures; i++) {
			textures[i] = 0;
		}
		
		for (int i = 0; i < objMaterials.size(); i++) {
			if(mtlMaterials.containsKey(objMaterials.get(i)))
				this.setTexture(i, "res/texture/" + mtlMaterials.get(objMaterials.get(i)));
			else
				this.setTexture(i, "empty");
		}
		
		return true;
	}
	
	private String getNextTexture(ArrayList<String> data, int i){
		for (int j = i; j < data.size(); j++) {
			String line = data.get(j).trim();
			
			if(line.startsWith("map_Kd ")){
				String str[] = line.split("\\s+");
				if(str.length > 1){
					return str[1].trim();
				}
			}
			else if(line.startsWith("newmtl ")){
				break;
			}
		}
		return "";
	}
}
