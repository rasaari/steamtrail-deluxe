package graphics.mesh;

import graphics.emitter.Particle;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

/**
 *
 * @author simokr
 */
public class EmitterMesh extends Mesh {
	private int vertexSize;
	
	public EmitterMesh(){
		super();
		setPrimitiveType(GL_POINTS);
		vertexSize = 6;
	}
	
	public boolean create(int maxParticles) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(maxParticles * 1 * this.vertexSize);

		if(!super.create(buffer, this.vertexSize, GL_STREAM_DRAW, true)){
			return false;
		}

		return true;
	}

	public boolean update(ArrayList<Particle> particles) {
		if(this.VBO != null){
			this.VBO.clear();
		}
		int posIndex = 0, otherIndex = particles.size()*3;
		float[] buffer = new float[particles.size()*6];
		
		Collections.sort(particles);
		for (Particle particle : particles) {
			float size = particle.size*5/particle.distanceToCamera();

			/* Triangle 1 */
			buffer[posIndex++] = particle.position.x;
			buffer[posIndex++] = particle.position.y;
			buffer[posIndex++] = particle.position.z;
			
			buffer[otherIndex++] = size;
			buffer[otherIndex++] = particle.getAlpha();
			buffer[otherIndex++] = particle.getTextureUnit();
		}
		this.VBO.put(buffer);
		this.VBO.flip();

		return super.update();
	}
	
	@Override
	protected void setVertexPointers(){
		glEnableVertexAttribArray(0);
		//glVertexAttribPointer(0, 3, GL_FLOAT, false, 24, 0);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
		int offset = this.indiceCount * 3 * 4;
		
		glEnableVertexAttribArray(2);
		//glVertexAttribPointer(2, 3, GL_FLOAT, false, 24, 12);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, offset);
	}
	
	@Override
	protected void unsetVertexPointers(){
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(2);
	}
}
