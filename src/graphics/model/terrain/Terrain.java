/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics.model.terrain;

import static engine.Engine.ShaderHandler;
import graphics.mesh.Mesh;
import graphics.model.Model;
import java.util.ArrayList;
import static org.lwjgl.opengl.GL15.*;

/**
 *
 * @author simokr
 */
public class Terrain extends Model{
	private final int width, depth;
	public Terrain(int width, int depth){
		super();
		mesh = new Mesh();
		this.width = width;
		this.depth = depth;
		shader = ShaderHandler.get("./res/shaders/terrain.vert|./res/shaders/terrain.frag");
		
		create();
	}
	
	private void create(){
		Mesh meshBuilder = new Mesh();
		ArrayList<Float> buffer = new ArrayList<>();
		
		float halfWidth = this.width/2;
		float halfDepth = this.depth/2;		
		
		buffer.add(-halfWidth);
		buffer.add(0f);
		buffer.add(-halfDepth);
		buffer.add(-halfWidth);
		buffer.add(0f);
		buffer.add(halfDepth);
		buffer.add(halfWidth);
		buffer.add(0f);
		buffer.add(halfDepth);
		buffer.add(-halfWidth);
		buffer.add(0f);
		buffer.add(-halfDepth);
		buffer.add(halfWidth);
		buffer.add(0f);
		buffer.add(halfDepth);
		buffer.add(halfWidth);
		buffer.add(0f);
		buffer.add(-halfDepth);
		
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(halfDepth);
		buffer.add(halfWidth);
		buffer.add(halfDepth);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(halfWidth);
		buffer.add(halfDepth);
		buffer.add(halfWidth);
		buffer.add(0f);
		
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(1f);
		buffer.add(0f);
		
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(0f);
		buffer.add(0f);
		
		meshBuilder.create(buffer, 9, GL_STATIC_DRAW, false);
		
		/* Release old mesh (if set) */ 
		if(this.mesh.isReady())
			this.free();
		
		this.setMesh(meshBuilder);
		this.material.loadFromFile("res/mesh/terrain.mtl");
		
		System.out.println("Created terrain - "+(buffer.size()/9/3)+" triangles.");
		buffer.clear();
	}
	
	@Override
	public void free(){
		this.mesh.free();
	}
}
