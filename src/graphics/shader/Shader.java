package graphics.shader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static graphics.Graphics.getViewMatrix;
import static graphics.Graphics.getProjectionMatrix;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;

/**
 *
 * @author simokr
 */
public class Shader {
	private String vertPath, fragPath;
	private boolean ready;
	private int program, vertShader, fragShader;
	private int viewMatrixLocation, modelMatrixLocation, projectionMatrixLocation, keyframeBlendLocation;
	private final FloatBuffer modelMatrixBuffer;
	
	public Shader(){
		ready = false;
		program = 0;
		vertShader = 0;
		fragShader = 0;
		viewMatrixLocation = 0;
		modelMatrixLocation = 0;
		projectionMatrixLocation = 0;
		keyframeBlendLocation = 0;
		modelMatrixBuffer = BufferUtils.createFloatBuffer(16);
	}
	
	public void enable(){
		glUseProgram(this.program);
		glUniformMatrix4(this.viewMatrixLocation, false, getViewMatrix());
		glUniformMatrix4(this.modelMatrixLocation, false, this.modelMatrixBuffer);
		glUniformMatrix4(this.projectionMatrixLocation, false, getProjectionMatrix());
	}
	
	public void setKeyframeBlend(float blend){
		glUniform1f(this.keyframeBlendLocation, blend);
	}
	
	public void setModelMatrix(Matrix4f modelMatrix){
		modelMatrix.store(modelMatrixBuffer);
		modelMatrixBuffer.flip();
	}
	
	public void disable(){
		// Override this
		glUseProgram(0);
	}
	
	public boolean isReady(){
		return this.ready;
	}
	
	public boolean create(String vertPath, String fragPath){
		if(vertPath == null || fragPath == null || vertPath.isEmpty() || fragPath.isEmpty())
			return false;
		
		this.vertPath = vertPath;
		this.fragPath = fragPath;
		
		this.ready = this.createProgram();
		this.deleteShaderObjects();
		
		if(this.ready == false){
			this.deleteProgram();
		}
		
		return this.ready;
	}
	
	public void free(){
		this.deleteProgram();
		
		this.ready = false;
		this.program = 0;
		this.vertShader = 0;
		this.fragShader = 0;
		this.vertPath = "";
		this.fragPath = "";
	}
	
	private boolean createProgram(){
		this.vertShader = this.createShader(GL_VERTEX_SHADER, this.vertPath);
		this.fragShader = this.createShader(GL_FRAGMENT_SHADER, this.fragPath);
		
		if(this.vertShader == 0 || this.fragShader == 0) {
			return false;
		}
		
		this.program = glCreateProgram();

		if(this.program == 0) {
		       return false;
		}

		glAttachShader(this.program, this.vertShader);
		glAttachShader(this.program, this.fragShader);
		
		/* Set Attributes */
		this.setAttributes();
		glLinkProgram(this.program);
        
		int linkStatus;
		linkStatus = glGetProgrami(this.program, GL_LINK_STATUS);
		if(linkStatus == GL_FALSE){
			System.err.println("Shader program link status false.");
			this.printProgramLog(this.program);
			return false;
		}

		glValidateProgram(this.program);
		this.printProgramLog(this.program);

		this.preSetUniforms();
		glUseProgram(this.program);

		/* Set Uniforms */
		this.setUniforms();

		glUseProgram(0);
		
		System.out.println("Loaded " + this.vertPath + " & " + this.fragPath);
		
		return true;
	}
	
	private void setAttributes(){
		glBindAttribLocation(this.program, 0, "OBJ_Position");
		glBindAttribLocation(this.program, 1, "OBJ_Texcoord");
		glBindAttribLocation(this.program, 2, "OBJ_Normal");
		glBindAttribLocation(this.program, 3, "OBJ_TexUnit");
		glBindAttribLocation(this.program, 4, "OBJ_Position_B");
		glBindAttribLocation(this.program, 5, "OBJ_Normal_B");
	}
	
	private void setUniforms(){
		//Overload this

		int location0;
		location0 = glGetUniformLocation(this.program, "Difftexture0");
		glUniform1i(location0, 0);
		int location1;
		location1 = glGetUniformLocation(this.program, "Difftexture1");
		glUniform1i(location1, 1);
		int location2;
		location2 = glGetUniformLocation(this.program, "Difftexture2");
		glUniform1i(location2, 2);
		int location3;
		location3 = glGetUniformLocation(this.program, "Difftexture3");
		glUniform1i(location3, 3);
		
		this.viewMatrixLocation = glGetUniformLocation(this.program, "ViewMatrix");
		this.modelMatrixLocation = glGetUniformLocation(this.program, "ModelMatrix");
		this.projectionMatrixLocation = glGetUniformLocation(this.program, "ProjectionMatrix");
		this.keyframeBlendLocation = glGetUniformLocation(this.program, "KeyframeBlend");
		glUniform1f(this.keyframeBlendLocation, 0.0f);
	}
	
	private void preSetUniforms(){
	}
	
	private int createShader(int type, String path){
		int shader;

		shader = glCreateShader(type);

		if(shader == 0) {
			return 0;
		}

		String shaderCode = this.getSource(path);
		
		glShaderSource(shader, shaderCode);
		glCompileShader(shader);

		int shaderStatus;
		shaderStatus = glGetShaderi(shader,GL_COMPILE_STATUS);
		return shader;
	}
	
	public void refresh(){
		this.deleteProgram();

		this.create(this.vertPath, this.fragPath);
	}
	
	private void deleteProgram(){
		if(glIsProgram(this.program))
			glDeleteProgram(this.program);
	}
	
	private void deleteShaderObjects(){
		if(glIsShader(this.vertShader)){
			glDetachShader(this.program, this.vertShader);
			glDeleteShader(this.vertShader);
		}

		if(glIsShader(this.fragShader)){
			glDetachShader(this.program, this.fragShader);
			glDeleteShader(this.fragShader);
		}
	}
	
	private String getSource(String name){
		//path to map file
		String path = name;
		StringBuilder source = new StringBuilder();
		File file = new File(path);
		if (file.exists()){
			try {
				BufferedReader reader = new BufferedReader(new FileReader(path));
				
				try {
					String line;
					while((line = reader.readLine()) != null) {
						source.append(line).append('\n');
					}
				} catch (IOException ex) {
					Logger.getLogger(Shader.class.getName()).log(Level.SEVERE, null, ex);
				}
				finally{
					try {
						reader.close();
					} catch (IOException ex) {
						Logger.getLogger(Shader.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
				
			} catch (FileNotFoundException ex) {
				Logger.getLogger(Shader.class.getName()).log(Level.SEVERE, null, ex);
			}
			
			return source.toString();
		}
		return "";
	}
	
	private void printProgramLog(int obj){
		int maxLength = 0;

		maxLength = glGetProgrami(obj,GL_INFO_LOG_LENGTH);

		String infoLog;

		infoLog = glGetProgramInfoLog(obj, maxLength);

		if (!infoLog.isEmpty()){
			System.err.println(infoLog);
		}
	}
}
