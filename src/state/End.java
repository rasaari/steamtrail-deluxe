package state;

import de.lessvoid.nifty.elements.render.TextRenderer;
import engine.State;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class End extends State {

	/**
	 * Score is the length player travelled during game
	 *
	 * @param score
	 */
	public End(float score) {
		System.out.println("Game over. Score: " + score);
	}

	@Override
	public void init() {
	}

	@Override
	public void end() {
	}

	@Override
	public void initGui() {
		gui.gotoScreen("endScreen");
	}

	@Override
	public void render() {
		gui.getCurrentScreen()
			.findElementByName("score_list")
			.getRenderer(TextRenderer.class)
			.setText("\n1. VR - 999999\n2. VR - 999999\n3. VR - 999999\n4. VR - 999999\n5. VR - 999999");
	}

	@Override
	public void update() {
	}

	@Override
	public void paused() {
	}

	@Override
	public void event() {
	}
}
