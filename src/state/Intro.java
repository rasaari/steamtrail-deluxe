package state;

import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import engine.Animator;
import static engine.Engine.MeshHandler;
import static engine.Engine.TextureHandler;
import static engine.Engine.appSpeed;
import static engine.Engine.elapsedAppTime;
import engine.State;
import engine.StateManager;
import graphics.emitter.Emitter;
import graphics.mesh.Mesh;
import graphics.model.Model;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.vector.Vector3f;
/**
 *
 * @author Kami Nasri
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class Intro extends State {
	private boolean proceedToGame = false;
	Element niftyElement;
	
	//Intro text animation
	boolean isFaded = false;
	float alphaCounter=0f;
	
	private Model train[], rail[], station[], cactus[];
	private Emitter smokeEmitter;
	private Animator trainAnimator = new Animator(36, 0.2f);
	@Override
	public void init() {
		camera.setFieldOfView(40);
		camera.setPosition(-2f, 1, 2f);
		camera.lookAt(0, 0.7f, 0);
		
		train = new Model[3];
		train[0] = new Model();
		train[0].setMesh(MeshHandler.get("res/mesh/penydarren/penydarren.obj"));
		train[0].setPosition(0,0,-0.2f);
		//train[0].setRotation(0, 180, 0);
		train[1] = new Model();
		train[1].setMesh(MeshHandler.get("res/mesh/coach1.obj"));
		train[1].setPosition(0,0,-1.5f);
		train[2] = new Model();
		train[2].setMesh(MeshHandler.get("res/mesh/coach1.obj"));
		train[2].setPosition(0,0,-2.5f);
		
		smokeEmitter = new Emitter(3, 4000);
		smokeEmitter.setVelocity(new Vector3f(0, 1.6f, 0));
		smokeEmitter.setSizes(5, 500);
		smokeEmitter.setSizesRandomness(1, 100);
		smokeEmitter.setAcceleration(new Vector3f(3f, -0.05f, -0.3f));
		smokeEmitter.setTranslation(new Vector3f(0, 0.08f, 0.30f));
		smokeEmitter.setPosition(0, 0.6f, -0.5f);
		
		Mesh track = MeshHandler.get("res/mesh/track.obj");
		Mesh track90 = MeshHandler.get("res/mesh/track_90_corner.obj");
		Mesh stationMesh = MeshHandler.get("res/mesh/station2.obj");
		Mesh stationBin = MeshHandler.get("res/mesh/station2_bin.obj");
		Mesh stationMap = MeshHandler.get("res/mesh/station2_map.obj");
		
		station = new Model[6];
		for(int i=0;i<6;i++){
			station[i] = new Model();
			station[i].setMesh(stationMesh);
			station[i].setRotation(0, 180, 0);
		}
		station[1].setPosition(0, 0, -1);
		station[2].setPosition(0, 0, -2);
		station[3].setMesh(stationBin);
		station[3].setPosition(1.25f, 0, 0);
		station[3].setRotation(0, 0, 0);
		station[4].setMesh(stationMap);
		station[4].setPosition(0, -0.05f, 0);
		station[5].setMesh(MeshHandler.get("res/mesh/station2_bench.obj"));
		station[5].setPosition(-0.1f,0,-0.2f);
	
		
		rail = new Model[20];
		for(int i=0;i<20;i++){
			rail[i] = new Model();
			rail[i].setMesh(track);
			rail[i].setPosition(0,-5,0);
		}
		rail[0].setPosition(0,0,0);
		rail[1].setPosition(0,0,-1);
		rail[2].setPosition(0,0,-2);
		rail[3].setPosition(0,0,-3);
		rail[4].setPosition(0,0,-4);
		rail[5].setPosition(0,0,-5);
		rail[5].setRotation(0,-90f,0);
		rail[5].setMesh(track90);
		rail[6].setPosition(-1,0,-5);
		rail[6].setRotation(0,-90f,0);
		rail[7].setPosition(0,0,1);
		rail[8].setPosition(0,0,2);
		rail[9].setPosition(0,0,3);
		
		cactus = new Model[4];
		for(int i=0;i<4;i++){
			cactus[i] = new Model();
			cactus[i].setPosition(8, 0, -2);
			cactus[i].setRotation(0, 5f, 0);
		}
		cactus[0].setMesh(MeshHandler.get("res/mesh/cactus_trunk.obj"));
		cactus[1].setMesh(MeshHandler.get("res/mesh/cactus_branch1.obj"));
		cactus[2].setMesh(MeshHandler.get("res/mesh/cactus_branch2.obj"));
		cactus[3].setMesh(MeshHandler.get("res/mesh/cactus_branch3.obj"));
	}

	@Override
	public void initGui() {
		gui.gotoScreen("introScreen");
	}

	@Override
	public void render() {
		train[0].render();
		train[1].render();
		train[2].render();
		
		for(int i=0;i<station.length;i++){
			station[i].render();
		}
		
		for(int i=0;i<rail.length;i++)
			rail[i].render();
		
		for(int i=0;i<cactus.length;i++)
			cactus[i].render();
		
		smokeEmitter.render();
	}

	@Override
	public void update() {
		if(this.proceedToGame){
			StateManager.push((State) (new Game()));
		}
		trainAnimator.update();
		train[0].animate(
				trainAnimator.getFrameBend(), 
				trainAnimator.getFrame(),
				trainAnimator.getFrame()+1);
		
		smokeEmitter.update();
		niftyElement = gui.getCurrentScreen().findElementByName("info");
		setFlashingText();
		
		while (Keyboard.next()) {
			switch (Keyboard.getEventKey()) {
				case Keyboard.KEY_RETURN:
					niftyElement.getRenderer(TextRenderer.class).getColor().setAlpha(1f);
					niftyElement.getRenderer(TextRenderer.class).setText("\\#ff0000#Loading, hold your colors..");
					proceedToGame = true;
					break;
				case Keyboard.KEY_2:
					TextureHandler.freeAll();
					MeshHandler.freeAll();
				case Keyboard.KEY_1:
					System.gc();
					System.runFinalization();
					glFinish();
					glFlush();
					break;
			}
		}
	}

	@Override
	public void event() {
	}

	@Override
	public void end() {
	}

	@Override
	public void paused() {
	}
	
	private void setFlashingText() {
		niftyElement.getRenderer(TextRenderer.class).getColor().setAlpha((float)Math.abs(Math.sin(alphaCounter)));
		alphaCounter += 0.04f*appSpeed();
	}
}