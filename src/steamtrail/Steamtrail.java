package steamtrail;

import engine.Engine;

/**
 *
 * @author Jari Saaranen <rasaari@gmail.com>
 */
public class Steamtrail {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/* uncomment when deploying
		String os = System.getProperty("os.name");
		String path = Steamtrail.class.getResource("Steamtrail.class").toString();

		os = os.equalsIgnoreCase("linux")? "linux" : "windows";

		if(path.startsWith("jar:")) {
		System.setProperty("org.lwjgl.librarypath", new File("native/"+os).getAbsolutePath());
		}
		*/
		Engine engine = new Engine();

		engine.start();

		System.exit(0);

	}

}
